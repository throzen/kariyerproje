@extends('backend.layout.master')
@section('content')
    <div class="panel panel-flat">

        <div class="panel-body">
            {{ Form::model($category, array(
            'method' => 'PUT',
            'route' => ['category.update', $category->id],
            'class' => 'form-horizontal'
             ))  }}
            <div class="form-group">
                {{Form::label('Başlık',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('title',$category->title,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Üst Kategori',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    <select name="parent_id" id="parent_id" class="form-control">
                        <option value=""></option>
                        @foreach($categories as $cate)
                            <option value="{{ $cate->id}}"
                                    @if($cate->id == $category->parent_id) selected @else  @endif>{{ $cate->title}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Durum',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-3">
                    {{Form::select('status',array(1 => 'Aktif',0 => 'Pasif'),$category->status,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="text-right">
                {{Form::submit('Kaydet',['class' => 'btn btn-primary'])}}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection