@extends('backend.layout.master')
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
    @endsection
    @section('content')
            <!-- Basic datatable -->
    <div class="panel panel-flat">
        <table class="table datatable-basic">
            <thead>
            <tr>
                <th>ID</th>
                <th>Başlık</th>
                <th>Durum</th>
                <th class="text-center">İşlem</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
                <tr>
                    <td>{{$category->id}}</td>
                    <td>{{$category->title}}</td>
                    <td>{{$category->status}}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>{{link_to_route('category.edit','Edit',$category->id)}}</li>
                                    {{ Form::open(array('route' => array('category.destroy', $category->id), 'method' => 'delete')) }}
                                    {{Form::submit('Delete',array('class' => 'btn btn-danger btn-xs'))}}
                                    {{ Form::close() }}
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection