@extends('backend.layout.master')
@section('content')
    <div class="panel panel-flat">

        <div class="panel-body">
            {{  Form::open(array('action'=>'CategoryController@store', 'method' => 'POST', 'class' => 'form-horizontal'))  }}
            <div class="form-group">
                {{Form::label('Başlık',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('title',null,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Üst Kategori',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::select('parent_id',array(),null,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Durum',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-3">
                    {{Form::select('status',array(1 => 'Aktif',null => 'Pasif'),null,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="text-right">
                {{Form::submit('Kaydet',['class' => 'btn btn-primary'])}}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection