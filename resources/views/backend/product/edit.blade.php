@extends('backend.layout.master')
@section('content')
    <div class="panel panel-flat">
        <div class="panel-body">
            {{ Form::model($product, array(
            'method' => 'PUT',
            'route' => ['product.update', $product->id],
            'class' => 'form-horizontal'
             ))  }}
            <div class="form-group">
                {{Form::label('Barcode',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('barcode',$product->barcode,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Başlık',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('title',$product->title,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Açıklama',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::textarea('description',$product->description,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Ürün Miktarı',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('quantity',$product->quantity,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Kategori',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-4">
                    {{Form::select('categories[]',$categories->pluck('title','id'),null,['class' =>'form-control','multiple' => 'multiple' ])}}
                </div>
            </div>
            <div class="text-right">
                {{Form::submit('Kaydet',['class' => 'btn btn-primary'])}}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection