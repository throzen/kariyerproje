<div class="sidebar-content">

    <!-- User menu -->
    <div class="sidebar-user">
        <div class="category-content">
            <div class="media">
                <a href="#" class="media-left"><img src="assets/images/placeholder.jpg"
                                                    class="img-circle img-sm" alt=""></a>
                <div class="media-body">
                    <span class="media-heading text-semibold">Victoria Baker</span>
                    <div class="text-size-mini text-muted">
                        <i class="icon-pin text-size-small"></i> &nbsp;Santa Ana, CA
                    </div>
                </div>

                <div class="media-right media-middle">
                    <ul class="icons-list">
                        <li>
                            <a href="#"><i class="icon-cog3"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /user menu -->


    <!-- Main navigation -->
    <div class="sidebar-category sidebar-category-visible">
        <div class="category-content no-padding">
            <ul class="navigation navigation-main navigation-accordion">

                <!-- Main -->
                <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i>
                </li>
                <li>
                    <a href="{{url('/')}}"><i class="icon-home4"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#"><i class="icon-stack2"></i> <span>Kategori</span></a>
                    <ul>
                        <li><a href="{{url('/category')}}">Kategori</a></li>
                        <li><a href="{{url('/category/create')}}">Kategori Ekle</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-stack2"></i> <span>Depo</span></a>
                    <ul>
                        <li><a href="{{url('/store')}}">Depo</a></li>
                        <li><a href="{{url('/store/create')}}">Depo Ekle</a></li>
                        <li><a href="{{url('/store-product/create')}}">Depoya Ürün Ekle</a></li>
                        <li><a href="{{url('/store-product')}}">Depo Stok Durumu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="icon-stack2"></i> <span>Ürün</span></a>
                    <ul>
                        <li><a href="{{url('/product')}}">Ürün</a></li>
                        <li><a href="{{url('/product/create')}}">Ürün Ekle</a></li>
                    </ul>
                </li>
                <!-- /main -->
            </ul>
        </div>
    </div>
    <!-- /main navigation -->
</div>