@extends('backend.layout.master')
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
@endsection
@section('content')
    <div class="panel panel-flat">
        <table class="table datatable-basic">
            <thead>
            <tr>
                <th>ID</th>
                <th>Başlık</th>
                <th>Ürün Başlığı</th>
                <th>Adet</th>
                <th class="text-center">İşlem</th>
            </tr>
            </thead>
            <tbody>
            @foreach($stocks as $stock)
                <tr>
                    <td>{{$stock->id}}</td>
                    <td>{{$stock->title}}</td>
                    <td>{{$stock->product_title}}</td>
                    <td>{{$stock->quantity}}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>{{link_to_action('StoreProductController@edit','Edit',$stock->id)}}</li>
{{--                                    <li>{{link_to_action('StoreProductController@destroy','Edit',$stock->id)}}</li>--}}
                                    {{ Form::open(array('action' => array('StoreProductController@destroy', $stock->id), 'method' => 'delete')) }}
                                    {{Form::submit('Delete',array('class' => 'btn btn-danger btn-xs'))}}
                                    {{ Form::close() }}
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection