@extends('backend.layout.master')
@section('content')
    <div class="panel panel-flat">
        <div class="panel-body">
            {{  Form::open(array('action'=>'StoreProductController@store', 'method' => 'POST', 'class' => 'form-horizontal'))  }}
            <div class="form-group">
                {{Form::label('Ürün',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::select('product_id',$products->pluck('title','id'),null,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Depo',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::select('store_id',$stores->pluck('title','id'),null,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="text-right">
                {{Form::submit('Kaydet',['class' => 'btn btn-primary'])}}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection