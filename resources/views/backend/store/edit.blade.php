@extends('backend.layout.master')
@section('content')
    <div class="panel panel-flat">

        <div class="panel-body">
            {{  Form::model($store, array(
            'method' => 'PUT',
            'route' => ['store.update', $store->id],
            'class' => 'form-horizontal'
             )) }}
            <div class="form-group">
                {{Form::label('Başlık',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('title',$store->title,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Açıklama',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::textarea('description',$store->description,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Maksimum Stok Miktarı',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('amount_of_stock',$store->amount_of_stock,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="text-right">
                {{Form::submit('Kaydet',['class' => 'btn btn-primary'])}}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection