@extends('backend.layout.master')
@section('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/datatables_basic.js')}}"></script>
    @endsection
    @section('content')
            <!-- Basic datatable -->
    <div class="panel panel-flat">
        <table class="table datatable-basic">
            <thead>
            <tr>
                <th>ID</th>
                <th>Başlık</th>
                <th>Max. Stok Miktarı</th>
                <th class="text-center">İşlem</th>
            </tr>
            </thead>
            <tbody>
            @foreach($stores as $store)
                <tr>
                    <td>{{$store->id}}</td>
                    <td>{{$store->title}}</td>
                    <td>{{$store->amount_of_stock}}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>{{link_to_route('store.edit','Edit',$store->id)}}</li>
                                    {{ Form::open(array('route' => array('store.destroy', $store->id), 'method' => 'delete')) }}
                                    {{Form::submit('Delete',array('class' => 'btn btn-danger btn-xs'))}}
                                    {{ Form::close() }}
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection