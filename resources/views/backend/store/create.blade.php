@extends('backend.layout.master')
@section('content')
    <div class="panel panel-flat">

        <div class="panel-body">
            {{  Form::open(array('action'=>'StoreController@store', 'method' => 'POST', 'class' => 'form-horizontal'))  }}
            <div class="form-group">
                {{Form::label('Başlık',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('title',null,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Açıklama',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::textarea('description',null,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="form-group">
                {{Form::label('Maksimum Stok Miktarı',null,array('class' => 'control-label col-lg-2'))}}
                <div class="col-lg-10">
                    {{Form::text('amount_of_stock',null,['class' =>'form-control' ])}}
                </div>
            </div>
            <div class="text-right">
                {{Form::submit('Kaydet',['class' => 'btn btn-primary'])}}
            </div>

            {{ Form::close() }}
        </div>
    </div>
@endsection