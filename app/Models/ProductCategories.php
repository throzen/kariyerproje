<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    protected $table = 'product_categories', $primaryKey = 'id';

    protected $fillable = ['product_id', 'category_id'];
}
