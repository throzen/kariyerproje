<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category', $primaryKey = 'id';

    protected $fillable = ['parent_id', 'title', 'status'];
}
