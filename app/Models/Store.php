<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = 'store', $primaryKey = 'id';

    protected $fillable = ['title', 'description', 'amount_of_stock'];

}
