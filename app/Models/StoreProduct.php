<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoreProduct extends Model
{
    protected $table = 'product_stores', $primaryKey = 'id';

    protected $fillable = ['product_id', 'store_id'];
}
