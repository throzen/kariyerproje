<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('store', 'StoreController');
Route::resource('store-product', 'StoreProductController');
Route::resource('category', 'CategoryController');
Route::resource('product', 'ProductController');

Route::group(['domain' => 'api.kariyerproje.app', 'namespace' => 'Api\v1'], function () {
    Route::group(['prefix' => 'v1'], function () {
        Route::resource('category', 'CategoryController');
        Route::resource('store', 'StoreController');
        Route::resource('store-product', 'StoreProductController');
        Route::resource('product', 'ProductController');
    });
});
