<?php

namespace App\Http\Controllers;

use App\Repositories\StoreRepository;
use App\Services\ProductService;
use App\Services\StoreProductService;
use App\Services\StoreService;
use Illuminate\Http\Request;

use App\Http\Requests;

class StoreController extends Controller
{

    /**
     * @var StoreService
     */
    protected $service;

    /**
     * @var ProductService
     */
    protected $product_service;
    /**
     * @var StoreProductService
     */
    protected $product_stores;

    public function __construct(StoreService $storeService,
                                ProductService $productService,
                                StoreProductService $storeProductService)
    {
        $this->service = $storeService;
        $this->product_service = $productService;
        $this->product_stores = $storeProductService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = $this->service->getStores();
        return view('backend.store.index', compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->request->all();
        $return = $this->service->create($data);
        return $return->save() ? redirect('store') : redirect('store/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $store = $this->service->find($id);
        return view('backend.store.edit', compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data = $request->except('_token', '_method');
        $return = $this->service->update($data, $id);
        if ($return) {
            return redirect('store');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $return = $this->service->delete($id);
        if ($return) {
            return redirect('store');
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createProductStore()
    {
        $products = $this->product_service->getAll();
        $stores = $this->service->getStores();
        return view('backend.product.create_product', compact('products', 'stores'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addProductStore(Request $request)
    {
        $data = $request->request->all();
        $return = $this->product_stores->create($data);
        return $return->save() ? redirect('store') : redirect('store/add-product');
    }
}
