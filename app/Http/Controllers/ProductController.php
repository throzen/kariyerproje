<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use App\Services\ProductService;
use App\Services\StoreService;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
    protected $service, $store_service, $category_service;

    public function __construct(ProductService $productService,
                                StoreService $storeService,
                                CategoryService $categoryService)
    {
        $this->service = $productService;
        $this->store_service = $storeService;
        $this->category_service = $categoryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->service->getProducts();
        return view('backend.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $stores = $this->store_service->getStores();
        $categories = $this->category_service->getCategories();
        return view('backend.product.create', compact('stores', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->request->all();
        $return = $this->service->addProduct($data);
        return $return == true ? redirect('product') : redirect('product/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->service->find($id);
        $categories = $this->category_service->getCategories();
        return view('backend.product.edit', compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data = $request->except('_token', '_method');

        $return = $this->service->update($data, $id);
        if ($return) {
            return redirect('product');
        } else {
            return redirect('product.edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $return = $this->service->delete($id);
        if ($return) {
            return redirect('product');
        }
    }
}
