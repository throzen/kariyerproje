<?php

namespace App\Http\Controllers;

use App\Services\ProductService;
use App\Services\StoreProductService;
use App\Services\StoreService;
use Illuminate\Http\Request;

use App\Http\Requests;

class StoreProductController extends Controller
{
    protected $service;
    protected $store_service;
    protected $product_service;

    public function __construct(StoreProductService $storeProductService,
                                StoreService $storeService,
                                ProductService $productService)
    {
        $this->service = $storeProductService;
        $this->store_service = $storeService;
        $this->product_service = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stocks = $this->service->getProductsInStores();
        return view('backend.storeProduct.index', compact('stocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = $this->product_service->getAll();
        $stores = $this->store_service->getStores();
        return view('backend.storeProduct.create', compact('stores', 'products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->request->all();
        $return = $this->service->create($data);
        if ($return->save()) {
            return redirect('store-product');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = $this->service->find($id);
        $products = $this->product_service->getAll();
        $stores = $this->store_service->getStores();
        return view('backend.storeProduct.edit', compact('product','products','stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data = $request->except('_token', '_method');
        $return = $this->service->update($data, $id);
        if ($return) {
            return redirect('store-product');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $return = $this->service->delete($id);
        if ($return) {
            return redirect('store-product');
        }
    }
}
