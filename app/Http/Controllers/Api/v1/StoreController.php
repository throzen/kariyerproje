<?php

namespace App\Http\Controllers\Api\v1;

use App\Services\StoreService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Routing\Controller;

class StoreController extends Controller
{
    /**
     * @var StoreService
     */
    protected $service;

    public function __construct(StoreService $storeService)
    {
        $this->service = $storeService;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stores = $this->service->getStores();
        return response($stores, 200, ['Content-Type' => 'application/json']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->request->all();
        $return = $this->service->create($data);
        if ($return->save()) {
            return response($this->show($return->id), 200, ['Content-Type' => 'application/json']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->service->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $this->service->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->request->all();
        $return = $this->service->update($data, $id);
        if ($return) {
            return response(['Message' => 'Updated'], 200, ['Content-Type' => 'application/json']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $return = $this->service->delete($id);
        if ($return) {
            return response(['Message' => 'Deleted'], 200, ['Content-Type' => 'application/json']);
        }
    }
}
