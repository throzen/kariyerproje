<?php
/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 03:14
 */

namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class StoreProductRepository extends Repository
{
    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\StoreProduct';
    }


    public function getAll()
    {
        return $this->all();
    }

    public function getProductsInStores()
    {
        $sql = 'SELECT
	                ps.id,
                    s.title,
                    p.title AS product_title,
                    p.quantity
                FROM
                    product_stores AS ps
                INNER JOIN store AS s ON s.id = ps.store_id
                INNER JOIN product AS p ON p.id = ps.product_id';
        return \DB::select($sql);
    }

}