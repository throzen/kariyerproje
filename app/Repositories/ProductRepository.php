<?php
/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 03:14
 */

namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use DB;

class ProductRepository extends Repository
{
    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Product';
    }


    public function getAll()
    {
        return $this->all();
    }

    public function getProducts()
    {
        $sql = 'SELECT
                    product.*, c.title AS category_title,
                    s.title AS store_title
                FROM
                    product
                LEFT JOIN product_categories AS pc ON product.id = pc.product_id
                LEFT JOIN category AS c ON c.id = pc.category_id
                LEFT JOIN product_stores AS ps ON product.id = ps.product_id
                LEFT JOIN store AS s ON s.id = ps.store_id
                GROUP BY
                    product.id';
        return DB::select($sql);
    }

}