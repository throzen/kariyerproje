<?php
/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 03:14
 */

namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;

class CategoryRepository extends Repository
{
    /**
     * @return string
     */
    public function model()
    {
        return 'App\Models\Category';
    }


    public function getAll()
    {
        return $this->all();
    }

}