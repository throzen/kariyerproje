<?php

/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 01:24
 */

namespace App\Providers;

use Bosnadev\Repositories\Contracts\RepositoryInterface;
use Bosnadev\Repositories\Eloquent\Repository;

abstract class AbstractDataService implements RepositoryInterface
{

    /**
     * @var Repository
     */
    protected $repository;

    public function __construct($repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->repository->create($data);
    }

    /**
     * @param $columns array
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all($columns = array())
    {
        return $this->repository->all($columns);
    }

    /**
     * @param $data
     * @param $id
     * @return bool|int
     */
    public function update(array $data, $id)
    {
        return $this->repository->update($data, $id);
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        return $this->repository->delete($id);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        return $this->repository->find($id, $columns);
    }

    /**
     * @param $field
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findAllBy($field, $value, $columns = array('*'))
    {
        return $this->repository->findAllBy($field, $value, $columns);
    }

    /**
     * @param $field
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($field, $value, $columns = array('*'))
    {
        return $this->repository->findBy($field, $value, $columns);
    }

    /**
     * @param $where
     * @param array $columns
     * @return mixed
     */
    public function findWhere($where, $columns = array('*'))
    {
        return $this->repository->find($where, $columns);
    }


    public function paginate($perPage = 1, $columns = array('*'))
    {
        // TODO: Implement paginate() method.
    }

    public function saveModel(array $data)
    {
        return $this->repository->saveModel($data);
    }

    /**
     * @return mixed
     */
    abstract protected function getRepository();

}