<?php

/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 00:41
 */
namespace App\Services;

use App\Models\ProductCategories;
use App\Providers\AbstractDataService;
use App\Repositories\ProductCategoriesRepository;

class ProductCategoriesService extends AbstractDataService
{
    /**
     * ProductCategoriesService constructor.
     * @param ProductCategoriesRepository $productCategoriesRepository
     */
    public function __construct(ProductCategoriesRepository $productCategoriesRepository)
    {
        parent::__construct($productCategoriesRepository);
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->getRepository()->getAll();
    }

    /**
     * @return ProductCategoriesRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    public function addProductCategories($data)
    {
        $return = false;
        foreach ($data['categories'] as $item) {
            $product_categories['product_id'] = $data['product_id'];
            $product_categories['category_id'] = $item;
            $this->repository->create($product_categories);
            $return = true;
        }
        return $return;
    }


}