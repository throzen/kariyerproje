<?php

/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 00:41
 */
namespace App\Services;

use App\Providers\AbstractDataService;
use App\Repositories\StoreProductRepository;

class StoreProductService extends AbstractDataService
{
    /**
     * StoreProductRepository constructor.
     * @param StoreProductRepository $storeProductRepository
     */
    public function __construct(StoreProductRepository $storeProductRepository)
    {
        parent::__construct($storeProductRepository);
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->getRepository()->getAll();
    }

    /**
     * @return StoreProductRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    public function addProductStores($data)
    {
        $return = false;
        foreach ($data['stores'] as $item) {
            $product_stores['product_id'] = $data['product_id'];
            $product_stores['store_id'] = $item;
            $this->repository->create($product_stores);
            $return = true;
        }
        return $return;
    }

    public function addProductStore($data)
    {
        return $this->repository->create($data);
    }

    public function getProductsInStores()
    {
        return $this->getRepository()->getProductsInStores();
    }

}