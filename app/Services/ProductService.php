<?php

/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 00:41
 */
namespace App\Services;

use App\Providers\AbstractDataService;
use App\Repositories\ProductRepository;
use App\Services\ProductCategoriesService;
use App\Services\StoreProductService;
use DB;

class ProductService extends AbstractDataService
{
    protected $product_categories, $product_stores;

    /**
     * ProductService constructor.
     * @param ProductRepository $productRepository
     * @param ProductCategoriesService $productCategoriesService
     * @param StoreProductService $storeProductService
     */
    public function __construct(ProductRepository $productRepository,
                                ProductCategoriesService $productCategoriesService,
                                StoreProductService $storeProductService)
    {
        parent::__construct($productRepository);
        $this->product_categories = $productCategoriesService;
        $this->product_stores = $storeProductService;
    }

    public function getAll()
    {
        return $this->repository->all();
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->getRepository()->getProducts();
    }

    /**
     * @return ProductRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    public function addProduct($data)
    {
        $return = false;
        try {
            DB::beginTransaction();
            $product = $this->repository->create($data);
            if ($product->save()) {
                $data['product_id'] = $product->id;
                $this->product_categories->addProductCategories($data);
                DB::commit();
                $return = true;
            }

        } catch (\PDOException $e) {
            DB::rollBack();
            $return = false;
        }
        return $return;
    }


}