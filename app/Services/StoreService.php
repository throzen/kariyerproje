<?php
/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 16:02
 */

namespace App\Services;

use App\Providers\AbstractDataService;
use App\Repositories\StoreRepository;

class StoreService extends AbstractDataService
{
    /**
     * StoreService constructor.
     * @param StoreRepository $storeRepository
     */
    public function __construct(StoreRepository $storeRepository)
    {
        parent::__construct($storeRepository);
    }

    public function getStores()
    {
        return $this->repository->all();
    }

    /**
     * @return StoreRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }
}