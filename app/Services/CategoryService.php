<?php

/**
 * Created by IntelliJ IDEA.
 * User: throzen
 * Date: 06.06.2016
 * Time: 00:41
 */
namespace App\Services;

use App\Models\Category;
use App\Providers\AbstractDataService;
use App\Repositories\CategoryRepository;
use Bosnadev\Repositories\Contracts\RepositoryInterface;

class CategoryService extends AbstractDataService
{

    /**
     * CategoryService constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        parent::__construct($categoryRepository);
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->getRepository()->getAll();
    }

    /**
     * @return CategoryRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }


}